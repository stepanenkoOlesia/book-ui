import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { BookSearchModule } from "./modules/book-search/book-search.module";

const routes: Routes = [
  {
    path: "",
    redirectTo: "/search",
    pathMatch: "full",
  },
  {
    path: "search",
    loadChildren: () => BookSearchModule,
  },
  {
    path: "detail",
    loadChildren: () =>
      import("src/app/modules/book/book.module").then((m) => m.BookModule),
  },
  {
    path: "authors",
    loadChildren: () =>
      import("src/app/modules/author/author.module").then(
        (m) => m.AuthorModule
      ),
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
