import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BookRoutingModule } from './book-routing.module';
import { SharedModule } from "../../shared/shared.module";
import { BookCreateComponent } from './book-create/book-create.component';
import { BookEditComponent } from './book-edit/book-edit.component';
import { BookDetailFormComponent } from './book-detail-form/book-detail-form.component';


@NgModule({
  declarations: [
    BookCreateComponent,
    BookEditComponent,
    BookDetailFormComponent
  ],
  imports: [CommonModule, BookRoutingModule, SharedModule],
})
export class BookModule {}
