import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { BreadcrumbService } from "portal-lib";
import { MessageService } from "primeng/api";
import {
  AuthorDTO,
  AuthorsRESTAPIService,
  UpdateAuthorRequestParams,
} from "src/app/generated";
import { DetailFormComponent } from "../detail-form/detail-form.component";

@Component({
  selector: "app-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.scss"],
})
export class EditComponent implements OnInit {
  @ViewChild(DetailFormComponent, { static: false })
  public authorDetailFormComponent: DetailFormComponent;
  public helpArticleId = "PAGE_AUTHOR_EDIT";
  public translatedData: Record<string, string>;
  private readonly authorId: string;

  constructor(
    private readonly authorApi: AuthorsRESTAPIService,
    private readonly translateService: TranslateService,
    private readonly messageService: MessageService,
    private readonly route: ActivatedRoute,
    private readonly breadCrumbService: BreadcrumbService
  ) {
    this.authorId = String(this.route.snapshot.paramMap.get("id"));
  }

  ngOnInit(): void {
    this.translateService
      .get([
        "AUTHOR.EDIT.UPDATE_SUCCESS",
        "AUTHOR.EDIT.UPDATE_ERROR",
        "AUTHOR.HEADERS.EDIT",
      ])
      .subscribe((data) => {
        this.translatedData = data;
        this.breadCrumbService.setItems([
          {
            title: this.translatedData["AUTHOR.HEADERS.EDIT"],
            label: this.translatedData["AUTHOR.HEADERS.EDIT"],
          },
        ]);
      });
  }

  public onSumbit(data: AuthorDTO): void {
    const params: UpdateAuthorRequestParams = {
      id: this.authorId,
      authorCreateUpdateDTO: data,
    };
    this.authorApi.updateAuthor(params).subscribe(
      () => {
        this.messageService.add({
          severity: "success",
          summary: this.translatedData["AUTHOR.EDIT.UPDATE_SUCCESS"],
        });
      },
      () => {
        this.messageService.add({
          severity: "error",
          summary: this.translatedData["AUTHOR.EDIT.UPDATE_ERROR"],
        });
      }
    );
  }
}
