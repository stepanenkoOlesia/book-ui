import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { MessageService } from "primeng/api";
import { AuthorDTO, AuthorsRESTAPIService } from "src/app/generated";

@Component({
  selector: "app-detail-form",
  templateUrl: "./detail-form.component.html",
  styleUrls: ["./detail-form.component.scss"],
})
export class DetailFormComponent implements OnInit {
  @Output()
  public formSubmitted: EventEmitter<AuthorDTO> = new EventEmitter();
  public authorForm: FormGroup;
  public authorId: string = "";
  public helpArticleId = "PAGE_AUTHOR_DETAIL";
  public translatedData: Record<string, string>;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly route: ActivatedRoute,
    private readonly authorApi: AuthorsRESTAPIService,
    private readonly translateService: TranslateService,
    private readonly messageService: MessageService
  ) {
    this.authorId = String(this.route.snapshot.paramMap.get("id"));
    this.authorForm = formBuilder.group({
      firstName: new FormControl(null, Validators.required),
      surName: new FormControl(null, Validators.required),
      age: new FormControl(null, Validators.required),
    });
  }

  ngOnInit(): void {
    this.translateService.get(["AUTHOR.FORM_INVALID"]).subscribe((data) => {
      this.translatedData = data;
    });
    if (this.route.snapshot.paramMap.get("id")) {
      this.loadData(this.authorId);
    }
  }

  public emitForm(): void {
    if (this.authorForm.valid) {
      const authorDTO: AuthorDTO = this.authorForm.value;
      this.formSubmitted.emit(authorDTO);
    } else {
      this.messageService.add({
        severity: "error",
        summary: this.translatedData["AUTHOR.FORM_INVALID"],
      });
    }
  }

  private fillForm(authorDTO: AuthorDTO): void {
    Object.keys(this.authorForm.controls).forEach((formControl) => {
      Object.entries(authorDTO).map((key) => {
        if (formControl === key[0]) {
          this.authorForm.controls[formControl].setValue(key[1]);
        }
      });
    });
  }

  private loadData(authorID: string): void {
    this.authorApi.getAuthorById({ id: authorID }).subscribe((authors) => {
      this.fillForm(authors);
    });
  }
}
