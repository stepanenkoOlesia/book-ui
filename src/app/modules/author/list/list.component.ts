import { Component, Input, OnInit } from "@angular/core";
import {
  AuthorDTO,
  AuthorsRESTAPIService,
  GetAuthorByCriteriaRequestParams,
} from "src/app/generated";
import { Column } from "src/app/core/types/column";
import { ColumnType } from "src/app/core/types/columnType";
import { TranslateService } from "@ngx-translate/core";
import { MessageService } from "primeng/api";
import { BreadcrumbService } from "portal-lib";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Component({
  selector: "app-list",
  templateUrl: "./list.component.html",
  styleUrls: ["./list.component.scss"],
})
export class ListComponent implements OnInit {
  @Input()
  public results: AuthorDTO[];
  public criteria: GetAuthorByCriteriaRequestParams;
  public deleteDialogVisible = false;
  public helpArticleId = "PAGE_AUTHOR_SEARCH";
  public translatedData: Record<string, string>;

  private selectedAuthor = null;
  public columns: Column[] = [
    {
      field: "firstName",
      header: "FIRST_NAME",
      type: ColumnType.INPUT,
    },
    {
      field: "surName",
      header: "SUR_NAME",
      type: ColumnType.INPUT,
    },
    {
      field: "age",
      header: "AGE",
      type: ColumnType.INPUT,
    },
  ];

  constructor(
    private readonly authorApi: AuthorsRESTAPIService,
    private translateService: TranslateService,
    private messageService: MessageService,
    private breadCrumbService: BreadcrumbService
  ) {}

  ngOnInit(): void {
    this.translateService
      .get([
        "AUTHOR.DELETE.DELETE_SUCCESS",
        "AUTHOR.DELETE.DELETE_ERROR",
        "AUTHOR.HEADERS.LIST",
      ])
      .subscribe((data) => {
        this.translatedData = data;
        this.breadCrumbService.setItems([
          {
            title: this.translatedData["AUTHOR.HEADERS.LIST"],
            label: this.translatedData["AUTHOR.HEADERS.LIST"],
          },
        ]);
      });
    this.loadData();
  }

  public getSelectedAuthor(): any {
    return this.selectedAuthor;
  }

  public setSelectedAuthor(selectedAuthor: any): void {
    this.selectedAuthor = selectedAuthor;
  }

  public deleteTable(id: string): void {
    this.authorApi.deleteAuthor({ id }).subscribe(
      () => {
        this.messageService.add({
          severity: "success",
          summary: this.translatedData["AUTHOR.DELETE.DELETE_SUCCESS"],
        });
        this.loadData();
      },
      () => {
        this.messageService.add({
          severity: "error",
          summary: this.translatedData["AUTHOR.DELETE.DELETE_ERROR"],
        });
      }
    );
  }

  private loadData(): void {
    this.getTables().subscribe((tables) => {
      console.log("IN loadData", tables);
      this.results = tables;
    });
  }

  private getTables(): Observable<AuthorDTO[]> {
    return this.authorApi
      .getAuthorByCriteria({})
      .pipe(map((data) => data.stream as AuthorDTO[]));
  }
}
