import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthorRoutingModule } from './author-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { ListComponent } from './list/list.component';
import { DetailFormComponent } from './detail-form/detail-form.component';


@NgModule({
  declarations: [
    CreateComponent,
    EditComponent,
    ListComponent,
    DetailFormComponent
  ],
  imports: [CommonModule, AuthorRoutingModule, SharedModule],
})
export class AuthorModule {}
