import { Component, OnInit, ViewChild } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { BreadcrumbService } from "portal-lib";
import { MessageService } from "primeng/api";
import {
  AuthorDTO,
  AuthorsRESTAPIService,
  CreateAuthorRequestParams,
} from "src/app/generated";
import { DetailFormComponent } from "../detail-form/detail-form.component";

@Component({
  selector: "app-create",
  templateUrl: "./create.component.html",
  styleUrls: ["./create.component.scss"],
})
export class CreateComponent implements OnInit {
  @ViewChild(DetailFormComponent, { static: false })
  public authorDetailFormComponent: DetailFormComponent;
  public translatedData: Record<string, string>;

  public helpArticleId = "PAGE_TABLE_CREATE";

  constructor(
    private readonly authorApi: AuthorsRESTAPIService,
    private readonly translateService: TranslateService,
    private readonly messageService: MessageService,
    private readonly breadCrumbService: BreadcrumbService
  ) {}

  ngOnInit(): void {
    this.translateService
      .get([
        "AUTHOR.CREATE.CREATE_SUCCESS",
        "AUTHOR.CREATE.CREATE_ERROR",
        "AUTHOR.HEADERS.CREATE",
      ])
      .subscribe((data) => {
        this.translatedData = data;
        this.breadCrumbService.setItems([
          {
            title: this.translatedData["AUTHOR.HEADERS.CREATE"],
            label: this.translatedData["AUTHOR.HEADERS.CREATE"],
          },
        ]);
      });
  }

  public onSubmit(data: AuthorDTO): void {
    const params: CreateAuthorRequestParams = {
      authorCreateUpdateDTO: data,
    };
    this.authorApi.createAuthor(params).subscribe(
      () => {
        this.messageService.add({
          severity: "success",
          summary: this.translatedData["AUTHOR.CREATE.CREATE_SUCCESS"],
        });
      },
      () => {
        this.messageService.add({
          severity: "error",
          summary: this.translatedData["AUTHOR.CREATE.CREATE_ERROR"],
        });
      }
    );
  }
}
