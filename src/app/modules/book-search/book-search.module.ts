import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { BookSearchRoutingModule } from "./book-search-routing.module";
import { SharedModule } from "../../shared/shared.module";
import { CriteriaComponent } from "./criteria/criteria.component";
import { ResultsComponent } from "./results/results.component";
import { BookSearchComponent } from "./book-search.component";

@NgModule({
  declarations: [CriteriaComponent, ResultsComponent, BookSearchComponent],
  imports: [CommonModule, BookSearchRoutingModule, SharedModule],
  exports: [BookSearchComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class BookSearchModule {}
