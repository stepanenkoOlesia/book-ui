import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {
  AuthorDTO,
  AuthorsRESTAPIService,
  GetBooksByCriteriaRequestParams,
  CategoriesOfBooks,
} from "src/app/generated";
import { MessageService, SelectItem } from "primeng/api";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-criteria",
  templateUrl: "./criteria.component.html",
  styleUrls: ["./criteria.component.scss"],
})
export class CriteriaComponent implements OnInit {
  @Input() criteria: GetBooksByCriteriaRequestParams;
  @Output() public criteriaSubmitted =
    new EventEmitter<GetBooksByCriteriaRequestParams>();
  @Output() public resetEmitter = new EventEmitter();

  public helpArticleId = "PAGE_BOOKS_SEARCH";
  public translatedData: Record<string, string>;
  public canceledOptions: SelectItem[];
  public criteriaGroup: FormGroup;
  public authorIds: SelectItem[];
  public cateoriesOfBook: SelectItem[];

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly authorRestApi: AuthorsRESTAPIService,
    private readonly translateService: TranslateService,
    private readonly messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.translateService
      .get(["GENERAL.YES", "GENERAL.NO"])
      .subscribe((data) => {
        this.translatedData = data;
        this.canceledOptions = [
          { label: this.translatedData["GENERAL.YES"], value: true },
          { label: this.translatedData["GENERAL.NO"], value: false },
        ];
      });
    this.initFormGroup();
    this.loadAllAuthorsIds();
    this.loadAllCateoriesOfBook();
  }

  public submitCriteria(): void {
    if (this.criteriaGroup.invalid) {
      this.messageService.add({
        severity: "error",
        // summary:
        //   this.translatedData["BOOKS.BOOKS_SEARCH.CRITERIA.INVALID_EMAIL"],
      });
    }
    this.criteriaSubmitted.emit(this.criteriaGroup.value);
  }

  public resetFormGroup(): void {
    this.resetEmitter.emit();
  }

  private initFormGroup(): void {
    this.criteriaGroup = this.formBuilder.group({
      bookTitle: null,
      bookIsbn: [null, Validators.required, Validators.pattern("^[0-9]*$")],
      numberOfPages: null,
      categoryOfBook: null,
      authorId: null,
    });
  }
  private loadAllAuthorsIds(): void {
    this.authorRestApi.getAuthorByCriteria({}).subscribe((pageResult) => {
      const authors = pageResult.stream as AuthorDTO[];
      // this.authorIds =
      authors.map(
        (author) => console.log(author)
        //  ({
        //   label: author.id.toString(),
        //   value: author.id,
        // })
      );
    });
  }
  private loadAllCateoriesOfBook(): void {
    Object.entries(CategoriesOfBooks).map((key) => console.log(key));
    this.cateoriesOfBook = Object.keys(CategoriesOfBooks).map((key) => ({
      // label: CategoriesOfBooks[key],
      value: key,
    }));
  }
}
