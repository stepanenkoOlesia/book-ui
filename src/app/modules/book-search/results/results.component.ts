import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { Column } from "src/app/core/types/column";
import { ColumnType } from "src/app/core/types/columnType";
import { AuthorDTO, BookDTO } from "src/app/generated";

@Component({
  selector: "app-results",
  templateUrl: "./results.component.html",
  styleUrls: ["./results.component.scss"],
})
export class ResultsComponent {
  @Input() public results: BookDTO[];
  @Output() deleteEvent: EventEmitter<any> = new EventEmitter();

  public helpArticleId = "PAGE_BOOKS_SEARCH";

  public deleteDialogVisible = false;
  public selectedBook: BookDTO | {};
  public readonly pathToTranslationJSON = "BOOKS.BOOK_SEARCH.TABLE.HEADER.";

  public columns: Column[] = [
    {
      field: "title",
      header: this.pathToTranslationJSON + "TITLE",
      type: ColumnType.INPUT,
    },
    {
      field: "isbn",
      header: this.pathToTranslationJSON + "ISBN",
      type: ColumnType.INPUT,
    },
    {
      field: "numberOfPages",
      header: this.pathToTranslationJSON + "NUMBER_OF_PAGES",
      type: ColumnType.INPUT,
    },
    {
      field: "category",
      header: this.pathToTranslationJSON + "CATEGORY",
      type: ColumnType.DROPDOWN,
    },
    {
      field: "author",
      header: this.pathToTranslationJSON + "AUTHOR_ID",
      type: ColumnType.DROPDOWN,
    },
  ];

  public getAuthorId(author: any): number {
    return author.id;
  }
  public getSelectedBook(): BookDTO {
    return this.selectedBook;
  }

  public setSelectedBook(selectedBook: BookDTO): void {
    this.selectedBook = selectedBook;
  }
}
