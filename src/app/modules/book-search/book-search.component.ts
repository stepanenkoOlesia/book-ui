import { Component, OnInit } from "@angular/core";
import { BreadcrumbService, PortalSearchPage, provideParent } from "portal-lib";
import {
  AuthorsRESTAPIService,
  BookDTO,
  BooksRESTAPIService,
  GetBooksByCriteriaRequestParams,
} from "../../generated";
import { map } from "rxjs/operators";
import { Observable } from "rxjs";
import { MessageService, SelectItem } from "primeng/api";
import { TranslateService } from "@ngx-translate/core";
@Component({
  selector: "app-book-search",
  templateUrl: "./book-search.component.html",
  styleUrls: ["./book-search.component.scss"],
  providers: [provideParent(BookSearchComponent)],
})
export class BookSearchComponent
  extends PortalSearchPage<BookDTO, GetBooksByCriteriaRequestParams>
  implements OnInit
{
  public criteria: GetBooksByCriteriaRequestParams;
  // public tablesIds: SelectItem[];

  public helpArticleId = "PAGE_BOOKS_SEARCH";
  public translatedData: Record<string, string>;

  public bookingTypes: SelectItem[];
  constructor(
    private readonly bookRestApi: BooksRESTAPIService,
    private readonly breadCrumbService: BreadcrumbService,
    private readonly translateService: TranslateService,
    private readonly messageService: MessageService,
    private readonly authorRestApi: AuthorsRESTAPIService
  ) {
    super();
  }

  ngOnInit(): void {
    this.criteria = this.getDefaultCriteria();
    this.translateService
      .get([
        "BOOKS.BOOKS_DELETE.BOOKS_DELETE_SUCESS",
        "BOOKS.BOOKS_DELETE.BOOKS_DELETE_ERROR",
        "BOOKS.BOOKS_CREATE_EDIT.CREATE_BOOKS",
      ])
      .subscribe((data) => {
        this.translatedData = data;
        this.breadCrumbService.setItems([
          {
            title: this.translatedData["BOOKS.BOOKS_CREATE_EDIT.CREATE_BOOKS"],
            label: this.translatedData["BOOKS.BOOKS_CREATE_EDIT.CREATE_BOOKS"],
          },
        ]);
      });
  }

  doSearch(): Observable<BookDTO[]> {
    return this.bookRestApi
      .getBooksByCriteria(this.criteria)
      .pipe(map((pageResult) => pageResult.stream as BookDTO[]));
  }

  public submitCriteria(criteria: GetBooksByCriteriaRequestParams): void {
    this.criteria = criteria;
    this.search();
  }

  getDefaultCriteria(): GetBooksByCriteriaRequestParams {
    return {};
  }

  public deleteBook(id: number): void {
    this.bookRestApi.deleteBook({ id }).subscribe(
      () => {
        this.messageService.add({
          severity: "success",
          summary:
            this.translatedData["BOOKS.BOOKS_DELETE.BOOKS_DELETE_SUCESS"],
        });
        this.search();
      },
      () => {
        this.messageService.add({
          severity: "error",
          summary: this.translatedData["BOOKS.BOOKS_DELETE.BOOKS_DELETE_ERROR"],
        });
      }
    );
  }

  public resetEmitted(): void {
    this.reset();
  }
}
