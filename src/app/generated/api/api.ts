export * from './authorsREST.service';
import { AuthorsRESTAPIService } from './authorsREST.service';
export * from './booksREST.service';
import { BooksRESTAPIService } from './booksREST.service';
export const APIS = [AuthorsRESTAPIService, BooksRESTAPIService];
