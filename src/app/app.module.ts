import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import {
  APP_CONFIG,
  AuthModule,
  MockAuthModule,
  StandardTranslateHttpLoader,
  TkitPortalModule,
} from "portal-lib";
// import { BASE_PATH } from "./generated";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { environment } from "../environments/environment";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { HttpClient } from "@angular/common/http";
import { MessageService } from "primeng/api";
import { FormsModule } from "@angular/forms";

const authModule = environment.production ? AuthModule : MockAuthModule;

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    AuthModule,
    MockAuthModule,
    TkitPortalModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: StandardTranslateHttpLoader,
        deps: [HttpClient],
      },
    }),
  ],
  providers: [
    { provide: APP_CONFIG, useValue: environment },
    MessageService,
    // { provide: BASE_PATH, useValue: "book-api" },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
