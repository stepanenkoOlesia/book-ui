FROM node:12.2.0 as build
COPY package*json package-lock.json .npmrc ./

## Storing node modules on a separate layer will prevent unnecessary npm installs at each build
RUN npm ci && mkdir /ng-app && mv ./node_modules ./ng-app

WORKDIR /ng-app
COPY . .
# you need to use `--` to pass params to npm scripts
RUN npm run ng build -- --prod --output-path=dist/
## Actual dist
FROM bitnami/nginx

USER root
RUN rm -rf /opt/bitnami/nginx/html/*
USER 101
## Copy our default nginx config
COPY nginx/default.conf /opt/bitnami/nginx/conf/server_blocks/

COPY --from=build --chown=101:0 /ng-app/dist /opt/bitnami/nginx/html


COPY entrypoint.sh /

USER root
RUN chmod 775 -R /opt/bitnami/nginx/html/assets && \
  chown 101 -R /opt/bitnami/nginx/conf/server_blocks/ && \
  chmod +x /entrypoint.sh
USER 101
ENTRYPOINT ["/bin/bash", "/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]

